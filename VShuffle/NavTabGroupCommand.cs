﻿//------------------------------------------------------------------------------
// <copyright file="NavTabGroupCommand.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.Platform.WindowManagement;
using Microsoft.VisualStudio.Shell;
using EnvDTE;
using EnvDTE80;

namespace VShuffle
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class NavTabGroupCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        private const int GotoLeftId = 0x0100;
        private const int GotoRightId = 0x0101;
        private const int MovetoLeftId = 0x0102;
        private const int MovetoRightId = 0x0103;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("dfb8ca29-60e9-46be-a228-0559beb5a49b");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Package package;

        private enum Direction { Left, Right }

        private readonly DTE2 dteService;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="NavTabGroupCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private NavTabGroupCommand(Package package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }

            this.package = package;

            this.dteService = ServiceProvider.GetService(typeof(DTE)) as DTE2;

            OleMenuCommandService commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                {
                    var menuCommand = new OleMenuCommand(this.GotoLeft, new CommandID(CommandSet, GotoLeftId));
                    menuCommand.BeforeQueryStatus += OnBeforeQueryStatus;
                    commandService.AddCommand(menuCommand);
                }
                {
                    var menuCommand = new OleMenuCommand(this.GotoRight, new CommandID(CommandSet, GotoRightId));
                    menuCommand.BeforeQueryStatus += OnBeforeQueryStatus;
                    commandService.AddCommand(menuCommand);
                }
                {
                    var menuCommand = new OleMenuCommand(this.MovetoLeft, new CommandID(CommandSet, MovetoLeftId));
                    menuCommand.BeforeQueryStatus += OnBeforeQueryStatus;
                    commandService.AddCommand(menuCommand);
                }
                {
                    var menuCommand = new OleMenuCommand(this.MovetoRight, new CommandID(CommandSet, MovetoRightId));
                    menuCommand.BeforeQueryStatus += OnBeforeQueryStatus;
                    commandService.AddCommand(menuCommand);
                }
            }
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static NavTabGroupCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private IServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            Instance = new NavTabGroupCommand(package);
        }

        private EnvDTE.Window FindActiveTab()
        {
            var activeDocument = dteService.ActiveDocument;
            return activeDocument?.ActiveWindow;
        }

        private DocumentView GetWindowDocumentView(EnvDTE.Window window)
        {
            if (window == null)
                throw new ArgumentNullException("window");

            if (window.Kind == "Document")
                return window.GetType().GetProperty("DockViewElement", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(window) as DocumentView;

            return null;
        }

        private List<EnvDTE.Window> GetSelectedTabsLeftToRight()
        {
            // Collect the left coordinate, the view group index and the window of each selected tab.
            var selectedTabs = new List<Tuple<int, int, EnvDTE.Window>>();
            foreach (EnvDTE.Window window in dteService.Windows)
            {
                DocumentView documentView = GetWindowDocumentView(window);
                if (documentView?.IsSelected == true)
                {
                    var viewGroup = documentView.Parent;
                    var viewGroupIndex = viewGroup.Parent.Children.IndexOf(viewGroup);
                    selectedTabs.Add(Tuple.Create(window.Left, viewGroupIndex, window));
                }
            }

            // Sort the selected tabs by left coordinate and then by view group index.
            var selectedTabsSorted = selectedTabs.OrderBy(tab => tab.Item1).ThenBy(tab => tab.Item2).ToList();

            // Drop the helper values and return only the sorted windows.
            return selectedTabsSorted.Select(tab => tab.Item3).ToList();
        }

        private EnvDTE.Window FindNextSelectedTab(EnvDTE.Window tab, Direction direction)
        {
            if (tab == null)
                throw new ArgumentNullException("window");

            var selectedTabs = GetSelectedTabsLeftToRight();
            if (selectedTabs.Count < 2)
                return null;

            var tabIndex = selectedTabs.FindIndex(tab.Equals);
            if (tabIndex == -1)
                return null;

            switch (direction)
            {
                case Direction.Left:
                    {
                        int nextTabIndex = tabIndex > 0 ? tabIndex - 1 : selectedTabs.Count - 1;
                        return selectedTabs[nextTabIndex];
                    }
                case Direction.Right:
                    {
                        int nextTabIndex = tabIndex < selectedTabs.Count - 1 ? tabIndex + 1 : 0;
                        return selectedTabs[nextTabIndex];
                    }
            }

            return null;
        }

        private bool MoveTab(EnvDTE.Window tab, Direction direction)
        {
            if (tab == null)
                throw new ArgumentNullException("tab");

            var nextTab = FindNextSelectedTab(tab, direction);
            if (nextTab == null)
                return false;

            var viewDocument = GetWindowDocumentView(tab);
            var nextViewDocument = GetWindowDocumentView(nextTab);

            var viewGroup = viewDocument.Parent;
            var nextViewGroup = nextViewDocument.Parent;

            viewGroup.Children.Remove(viewDocument);
            nextViewGroup.Children.Add(viewDocument);

            return true;
        }

        private void GotoLeft(object sender, EventArgs e)
        {
            FindNextSelectedTab(FindActiveTab(), Direction.Left)?.Activate();
        }

        private void GotoRight(object sender, EventArgs e)
        {
            FindNextSelectedTab(FindActiveTab(), Direction.Right)?.Activate();
        }

        private void MovetoLeft(object sender, EventArgs e)
        {
            var activeTab = FindActiveTab();
            if (MoveTab(activeTab, Direction.Left))
                activeTab.Activate();
        }

        private void MovetoRight(object sender, EventArgs e)
        {
            var activeTab = FindActiveTab();
            if (MoveTab(activeTab, Direction.Right))
                activeTab.Activate();
        }

        private void OnBeforeQueryStatus(object sender, EventArgs e)
        {
            var command = sender as OleMenuCommand;
            if (command != null)
            {
                var activeTab = FindActiveTab();
                command.Enabled = activeTab != null && activeTab.Kind == "Document";
            }
        }
    }
}
